package com.example.android.roverapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.roverapp.R;

/**
 * Created by Android on 08.12.2015.
 */
public class ExampleFragment extends Fragment {
    private static final int LAYOUT = R.layout.fragment_example_layout;
    View view;

    public static ExampleFragment getInstance(){
        Bundle bundle = new Bundle();
        ExampleFragment fragment = new ExampleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);
        return view;
    }
}
