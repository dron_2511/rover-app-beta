package com.example.android.roverapp.splash.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.android.roverapp.MainActivity;
import com.example.android.roverapp.R;
import com.example.android.roverapp.sign.SignActivity;

/**
 * Created by Android on 07.12.2015.
 */
public class SplashScreenActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    startActivity(new Intent(SplashScreenActivity.this, SignActivity.class));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        thread.start();

    }
}
