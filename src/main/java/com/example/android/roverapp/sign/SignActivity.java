package com.example.android.roverapp.sign;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.roverapp.MainActivity;
import com.example.android.roverapp.R;

public class SignActivity extends Activity {

    private final static String REGEXP_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Button btnSign, btnRegist;
    private EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_layout);

        getFindViewById();
    }

    public void getFindViewById() {
        btnSign = (Button) findViewById(R.id.btnSisn);
        btnRegist = (Button) findViewById(R.id.btnRegistration);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public boolean isEmail(String text) {
        return text.matches(REGEXP_EMAIL);
    }

    public void onClickSign(View view) {
        switch (view.getId()) {
            case R.id.btnSisn:
                String email = etEmail.getText().toString();
                if (email.length() > 0 && !isEmail(email)) {
                    Toast.makeText(this, "Не верный email", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnRegistration:
                Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
